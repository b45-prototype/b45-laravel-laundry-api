<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
Use Auth;
use App\Transformers\PostTransformer;

class PostController extends Controller
{
    public function create(Request $request, Post $post)
    {
        $this->validate($request, [
            'content'   => 'required|min:10'
        ]);
        
        $post = Post::create([
            'user_id'   => Auth::user()->id,
            'content'   => $request->content
        ]);

        $reponse = fractal()
            ->item($post)
            ->transformWith(new PostTransformer)
            ->toArray();
        
        return response()->json($reponse,201);
    }
}
