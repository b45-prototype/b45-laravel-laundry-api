<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;

use App\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request, User $user)
    {
        $this->validate($request,[
            'name'      =>'required',
            'username'  =>'required|unique:users',
            'email'     =>'email',
            'password'  =>'required|min:6',
            'telephone' =>'required'
        ]);

        $user->create([
            'name'      => $request->name,
            'username'  => $request->username,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'telephone' => $request->telephone,
            'api_token' => bcrypt($request->username)
        ]);

        $response = [
            'code' => 200,
            'status' => "success",
            'message' => "berhasil terdaftar"
        ];
        
        return response()->json($response, 201);
    }

    public function login(Request $request, User $user)
    {
        if(!Auth::attempt(['username' => $request->username, 'password' => $request->password])){
            return response()->json(['error' => 'Your credential is wrong', 401]);
        }

        $user = $user->find(Auth::user()->id);

        $response1 = [
            'code' => 200,
            'status' => "success",
            'user'   => $user
        ];
        $response2 = fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->addMeta([
                'token' => $user->api_token
            ])
            ->toArray();
        
        $responses= $response1+$response2;
        return response()->json($responses, 200);
    }
}
