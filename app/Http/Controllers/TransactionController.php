<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\TransactionDetail;
use App\Product;

use App\Transformers\TransactionTransformer;
use App\Transformers\TransactionDetailTransformer;
use App\Transformers\ProductTransformer;

use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function transactions(Transaction $transaction)
    {
        $transactions = $transaction->all();

        // return response()->json($transactions);
        return fractal()
            ->collection($transactions)
            ->transformWith(new TransactionTransformer)
            ->toArray();
    }

    public function create(Request $request, Transaction $transaction)
    {
        $this->validate($request,[
            'pelanggan'  =>'required',
            'id_products'=>'required',
            'unit'  =>'required',
            'qty'   =>'required'
        ]);

        $data1 = Transaction::create([
            'pelanggan'  => $request->pelanggan
        ]);
        
        if($data1){    
            $id_transaction = $data1->id;
            $data2 = TransactionDetail::create([
                'id_transactions' => $id_transaction,
                'unit'  => $request->unit,
                'qty'  => $request->qty
            ]);
            
            if($data2){
                $id_transaction_detail = $request->id_products;
                $product = DB::table('products')->where('id', $id_transaction_detail)->first(); 
                $response4 = [
                    'id' => $request->id_products,
                    'nama' => $product->product_name,
                    'unit' =>$product->unit,
                    'harga'=>$product->price
                ];

                if($product){
                    $tagihan = (int)$product->price * (int)$request->qty;
                    DB::table('transactions')
                    ->where('id', $id_transaction)
                    ->update(['tagihan' => $tagihan]);

                    $responseTagihan = [$tagihan];
                }
            
            }
        }

        $response1 = [
            'code' => 200,
            'status' => "success"
        ];
        $response2 = fractal()
        ->item($data1)
        ->transformWith(new TransactionTransformer)
        ->toArray();

        $response3 = fractal()
        ->item($data2)
        ->transformWith(new TransactionDetailTransformer)
        ->toArray();
    
        $responses= $response1+$response2+$responseTagihan+$response3+$response4;

        $layanan = [
            'id' => $request->id_products,
            'nama' => $product->product_name,
            'unit' =>$product->unit,
            'harga'=>$product->price
        ];

        $detail = [
            'id_transactions' => $request->id_products,
            'unit'  => $request->unit,
            'qty'  => $request->qty,
            'layanan' => $layanan
        ];

        $data = [
            'id'        =>$id_transaction,
            'pelanggan' =>$request->pelanggan,
            'unit'      =>$request->unit,
            'tagihan'   =>$tagihan,
            'detail' => $detail
        ];

        $responseAll = [
            'code'  => 200,
            'status'=> "success",
            'data' => $data
        ];

        return response()->json($responseAll, 200);
    }



}
