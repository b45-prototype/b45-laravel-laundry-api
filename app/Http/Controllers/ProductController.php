<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;

use App\Product;
use App\Transformers\ProductTransformer;

use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function products(Product $product)
    {
        $products = $product->all();

        // return response()->json($products);
        return fractal()
            ->collection($products)
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function create(Request $request, Product $product)
    {
        $this->validate($request,[
            'product_name'  =>'required',
            'unit'  =>'required',
            'price' =>'required'
        ]);

        $data = Product::create([
            'product_name'  => $request->product_name,
            'unit'          => $request->unit,
            'price'         => $request->price
        ]);

        $response1 = [
            'code' => 200,
            'status' => "success"
        ];
        $response2 = fractal()
        ->item($data)
        ->transformWith(new ProductTransformer)
        ->toArray();
    
        $responses= $response1+$response2;
        return response()->json($responses, 200);
    }
}
