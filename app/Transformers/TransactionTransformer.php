<?php 

namespace App\Transformers;

use App\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    public function transform(Transaction $transaction)
    {
        return [
            'id'    => $transaction->id,
            'pelanggan'  => $transaction->pelanggan,
            'tagihan'  => $transaction->tagihan
        ];
    }
}
