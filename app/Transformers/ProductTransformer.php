<?php 

namespace App\Transformers;

use App\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id'    => $product->id,
            'nama'  => $product->product_name,
            'unit'  => $product->unit,
            'harga' => $product->price
        ];
    }
}
