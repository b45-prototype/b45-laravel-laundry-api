<?php 

namespace App\Transformers;

use App\TransactionDetail;
use League\Fractal\TransformerAbstract;

class TransactionDetailTransformer extends TransformerAbstract
{
    public function transform(TransactionDetail $transactionDetail)
    {
        return [
            'unit'    => $transactionDetail->unit,
            'qty'  => $transactionDetail->qty        ];
    }
}
