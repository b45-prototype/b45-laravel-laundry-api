-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 05, 2019 at 06:06 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(26, '2014_10_12_000000_create_users_table', 1),
(27, '2014_10_12_100000_create_password_resets_table', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1),
(29, '2019_10_05_004917_create_products_table', 1),
(30, '2019_10_05_022407_update_users_table_add_api_token_field', 1),
(31, '2019_10_05_143101_create_transactions_table', 1),
(32, '2019_10_05_143738_create_transaction_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `unit`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Cuci Kering12', 'Kg', '10000', '2019-10-05 09:02:22', '2019-10-05 09:02:22'),
(2, 'Setrika', 'Lembar', '50000', '2019-10-05 09:02:46', '2019-10-05 09:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pelanggan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagihan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `pelanggan`, `tagihan`, `created_at`, `updated_at`) VALUES
(1, 'Pelanggan1', NULL, '2019-10-05 08:24:51', '2019-10-05 08:24:51'),
(2, 'Pelanggan1', NULL, '2019-10-05 08:29:38', '2019-10-05 08:29:38'),
(3, 'Pelanggan5', NULL, '2019-10-05 08:34:09', '2019-10-05 08:34:09'),
(4, 'Pelanggan5', NULL, '2019-10-05 09:03:57', '2019-10-05 09:03:57'),
(5, 'Pelanggan5', NULL, '2019-10-05 09:04:23', '2019-10-05 09:04:23'),
(6, 'Pelanggan5', NULL, '2019-10-05 09:06:47', '2019-10-05 09:06:47'),
(7, 'Pelanggan5', NULL, '2019-10-05 09:08:49', '2019-10-05 09:08:49'),
(8, 'Pelanggan5', NULL, '2019-10-05 09:14:16', '2019-10-05 09:14:16'),
(9, 'Pelanggan5', NULL, '2019-10-05 09:18:54', '2019-10-05 09:18:54'),
(10, 'Pelanggan5', NULL, '2019-10-05 09:20:52', '2019-10-05 09:20:52'),
(11, 'Pelanggan5', NULL, '2019-10-05 09:22:04', '2019-10-05 09:22:04'),
(12, 'Pelanggan5', NULL, '2019-10-05 09:23:23', '2019-10-05 09:23:23'),
(13, 'Pelanggan5', NULL, '2019-10-05 09:25:53', '2019-10-05 09:25:53'),
(14, 'Pelanggan5', NULL, '2019-10-05 09:27:42', '2019-10-05 09:27:42'),
(15, 'Pelanggan5', NULL, '2019-10-05 09:29:43', '2019-10-05 09:29:43'),
(16, 'Pelanggan5', NULL, '2019-10-05 09:30:31', '2019-10-05 09:30:31'),
(17, 'Pelanggan5', NULL, '2019-10-05 09:30:58', '2019-10-05 09:30:58'),
(18, 'Pelanggan5', NULL, '2019-10-05 09:34:50', '2019-10-05 09:34:50'),
(19, 'Pelanggan5', NULL, '2019-10-05 09:35:09', '2019-10-05 09:35:09'),
(20, 'Pelanggan5', NULL, '2019-10-05 09:35:48', '2019-10-05 09:35:48'),
(21, 'Pelanggan5', NULL, '2019-10-05 09:36:30', '2019-10-05 09:36:30'),
(22, 'Pelanggan5', NULL, '2019-10-05 09:37:10', '2019-10-05 09:37:10'),
(23, 'Pelanggan5', NULL, '2019-10-05 09:38:18', '2019-10-05 09:38:18'),
(24, 'Pelanggan5', NULL, '2019-10-05 09:40:56', '2019-10-05 09:40:56'),
(25, 'Pelanggan5', NULL, '2019-10-05 09:42:09', '2019-10-05 09:42:09'),
(26, 'Pelanggan5', NULL, '2019-10-05 10:04:58', '2019-10-05 10:04:58'),
(27, 'Pelanggan5', NULL, '2019-10-05 10:05:58', '2019-10-05 10:05:58'),
(28, 'Pelanggan5', NULL, '2019-10-05 10:07:58', '2019-10-05 10:07:58'),
(29, 'Pelanggan5', NULL, '2019-10-05 10:09:45', '2019-10-05 10:09:45'),
(30, 'Pelanggan5', NULL, '2019-10-05 10:11:12', '2019-10-05 10:11:12'),
(31, 'Pelanggan5', '50000', '2019-10-05 10:26:23', '2019-10-05 10:26:23'),
(32, 'Pelanggan5', '50000', '2019-10-05 10:27:11', '2019-10-05 10:27:11'),
(33, 'Pelanggan5', '50000', '2019-10-05 10:27:15', '2019-10-05 10:27:15'),
(34, 'Pelanggan5', '50000', '2019-10-05 10:50:32', '2019-10-05 10:50:32'),
(35, 'Pelanggan5', '50000', '2019-10-05 10:57:19', '2019-10-05 10:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE `transaction_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_transactions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_details`
--

INSERT INTO `transaction_details` (`id`, `id_transactions`, `unit`, `qty`, `created_at`, `updated_at`) VALUES
(10, '1', 'Kg', '5', '2019-10-05 09:40:56', '2019-10-05 09:40:56'),
(11, '25', 'Kg', '5', '2019-10-05 09:42:09', '2019-10-05 09:42:09'),
(12, '26', 'Kg', '5', '2019-10-05 10:04:58', '2019-10-05 10:04:58'),
(13, '27', 'Kg', '5', '2019-10-05 10:05:58', '2019-10-05 10:05:58'),
(14, '28', 'Kg', '5', '2019-10-05 10:07:58', '2019-10-05 10:07:58'),
(15, '29', 'Kg', '5', '2019-10-05 10:09:45', '2019-10-05 10:09:45'),
(16, '30', 'Kg', '5', '2019-10-05 10:11:12', '2019-10-05 10:11:12'),
(17, '31', 'Kg', '5', '2019-10-05 10:26:23', '2019-10-05 10:26:23'),
(18, '32', 'Kg', '5', '2019-10-05 10:27:11', '2019-10-05 10:27:11'),
(19, '33', 'Kg', '5', '2019-10-05 10:27:15', '2019-10-05 10:27:15'),
(20, '34', 'Kg', '5', '2019-10-05 10:50:32', '2019-10-05 10:50:32'),
(21, '35', 'Kg', '5', '2019-10-05 10:57:19', '2019-10-05 10:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
