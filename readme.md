## About b45-laravel-laundry-api

b45-laravel-laundry-api was made for my learning process in applying restfull API in laravel. At the same time to fulfill the assignment given for the job application that I sent.

## About Laundry Task/Test

- **[Details Task](https://drive.google.com/open?id=1Te6x8MMt-xGvy5fcBKGAwf2cysrVz4MO)**
- I have included some pictures in the commit comments that I made.

## Program outline Laundry Task/Test

- **[API register method POST reponse status API](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/c02b63df3e657a0b18b9b46edb6f6ede70841f41)**
- **[API login + response done, but not secure](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/c1f48652bf4a0e3151d1fd421c5119abb222a186)**
- **[API login + response more secure](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/760bb6c162906fa7d64bb7d555d9410e03790e45)**
- **[Product API / Layanan](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/826426875ac62c5a361f0f7f682ff4b4b2847c4b)**
- **[Testing Transaction,TransactionDetails,and Product Done](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/870b809ff6543f6857dad12fb5b97d00a70a0dd6)**
- **[Custom response API Transaction](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/4995ceb600319ad14583d94b0cf88f0cdb28e196)**
- **[Export database SQL](https://gitlab.com/b45-prototype/b45-laravel-laundry-api/commit/1477bfa899866fec8645b5120b213f0aff31dff5)**
- After export database SQL, time for work on assignments is done. I did not place "commit" in this Program outline

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
