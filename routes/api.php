<?php

use Illuminate\Http\Request;
// use Symfony\Component\Routing\Annotation\Route;
use Illuminate\Support\Facades\Route; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// my own route
// Just for test
Route::get('users','UserController@users');
Route::get('products','ProductController@products');
Route::get('transactions', 'TransactionController@transactions');

// register
Route::post('register','AuthController@register');

//login
Route::post('login','AuthController@login');

// Add product
Route::post('layanan','ProductController@create');

// Add transaction
Route::post('transaksi','TransactionController@create');

// User Profile
Route::get('users/profile','UserController@profile')->middleware('auth:api');
// Cek Profile By Id
Route::get('users/{id}','UserController@profileById')->middleware('auth:api');

// User Post Content
Route::post('post','PostController@create')->middleware('auth:api');